﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TomaszWojciechowskiLab5ZadanieDomowe.Models
{
    public class PizzaDbContext : DbContext
    {
        public PizzaDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<PizzaDbContext>(new PizzaDbInitializer());
        }

        public virtual DbSet<Pizza> Pizzas { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

    }
}