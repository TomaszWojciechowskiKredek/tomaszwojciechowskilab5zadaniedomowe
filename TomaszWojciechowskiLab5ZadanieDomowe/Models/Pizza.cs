﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TomaszWojciechowskiLab5ZadanieDomowe.Models
{
    public class Pizza
    {
        public int ID { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Składniki")]
        public string Components { get; set; }

        [Display(Name = "Cena")]
        public float Price { get; set; }

    }
}