﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TomaszWojciechowskiLab5ZadanieDomowe.Models
{
    public class Order
    {
        public int ID { get; set; }

        [Display(Name = "Numer")]
        public int Number { get; set; }

        [Display(Name = "Zamówiono")]
        public DateTime Ordered { get; set; }

        [Display(Name = "Dostarczono")]
        public DateTime Delivered { get; set; }
    }
}