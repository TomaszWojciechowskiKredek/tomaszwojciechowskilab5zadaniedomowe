﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace TomaszWojciechowskiLab5ZadanieDomowe.Models
{
    class PizzaDbInitializer : DropCreateDatabaseAlways<PizzaDbContext>
    {
        private PizzaDbContext context;

        protected override void Seed(PizzaDbContext con)
        {
            context = con;

            context.Pizzas.Add(new Pizza() { Name = "MARGHERITA", Components = "sos, ser, oregano", Price = 19.99f });
            context.Pizzas.Add(new Pizza() { Name = "SALAMI", Components = "sos, ser, salami", Price = 20.99f });
            context.Pizzas.Add(new Pizza() { Name = "MAFIA", Components = "sos, ser, czosnek, chili, tabasco, salami, papryka peperonii", Price = 21.99f });
            context.Pizzas.Add(new Pizza() { Name = "DANNY", Components = "sos, ser, kurczak, ogórek konserwowy, boczek (na serze) (sos bazyliowy)", Price = 22.99f });
            context.Pizzas.Add(new Pizza() { Name = "NOCNE PARTY", Components = "sos, ser, pieczarki, papryka, salami", Price = 23.99f });
            context.Pizzas.Add(new Pizza() { Name = "TEKSAS", Components = "sos, ser, papryka świeża, cebula, salami, (sos BBQ)", Price = 24.99f });
            context.Pizzas.Add(new Pizza() { Name = "KEBAB ", Components = "sos, ser, kebab, ser feta, cebula", Price = 25.99f });
            context.Pizzas.Add(new Pizza() { Name = "AFTER PARTY", Components = "sos, ser, tabasco, pieczarki, kebab, papryka świeża", Price = 26.99f });
            context.Pizzas.Add(new Pizza() { Name = "LUX", Components = "sos, ser, czosnek, boczek, krewetki, cebula, oliwki czarne", Price = 27.99f });

            context.Orders.Add(new Order() { Number = 119, Ordered = DateTime.Now.AddMinutes(-125), Delivered = DateTime.Now.AddMinutes(-80) });
            context.Orders.Add(new Order() { Number = 120, Ordered = DateTime.Now.AddMinutes(-120), Delivered = DateTime.Now.AddMinutes(-80) });
            context.Orders.Add(new Order() { Number = 121, Ordered = DateTime.Now.AddMinutes(-119), Delivered = DateTime.Now.AddMinutes(-80) });
            context.Orders.Add(new Order() { Number = 122, Ordered = DateTime.Now.AddMinutes(-51), Delivered = DateTime.Now.AddMinutes(-10) });
            context.Orders.Add(new Order() { Number = 123, Ordered = DateTime.Now.AddMinutes(-21), Delivered = DateTime.Now.AddMinutes(-22) });

            context.SaveChanges();

            base.Seed(context);
        }

    }
}
