﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TomaszWojciechowskiLab5ZadanieDomowe.Models;

namespace TomaszWojciechowskiLab5ZadanieDomowe.Controllers
{
    public class HomeController : Controller
    {
        PizzaDbContext context = new PizzaDbContext();

        public ActionResult Index()
        {
            ViewBag.Message = "Pyszna pizza w lokalu i na wynos.";

            var query = (from p in context.Pizzas
                         select p).ToList();

            return View(query);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Głodny wiedzy?";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Fajnie, ale jak tu trafić?";

            return View();
        }

        public ActionResult Status()
        {
            var query = (from p in context.Orders
                         select p).ToList();

            return View(query);
        }

        [HttpPost]
        public ActionResult Delivered(int orderID)
        {
            context.Orders.Where(o => o.ID == orderID).First().Delivered = DateTime.Now;
            context.SaveChanges();

            return RedirectToAction("Status");
        }

        [HttpPost]
        public ActionResult NewOrder()
        {
            int last = (from o in context.Orders
                        orderby o.Number descending
                            select o.Number).First();
            context.Orders.Add(new Order() { Number = last + 1, Ordered = DateTime.Now, Delivered = DateTime.Now.AddHours(-1) });
            context.SaveChanges();

            return RedirectToAction("Status");
        }
    }
}
